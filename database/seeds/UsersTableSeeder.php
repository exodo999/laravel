<?php

use Illuminate\Database\Seeder;
use Eloquent\Enumeration\AbstractEnumeration;
use App\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'administrator';
        $admin->email = 'admin@scriptmatico.com';
        $admin->password = bcrypt('111111');
        $admin->save();
    }
}