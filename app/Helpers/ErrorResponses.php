<?php
namespace App\Helpers;

trait ErrorResponses {

	public function userNoPermissions(){
		return array("errors"=>array("No enough privilegies"));
	}

	public function userNoAuthenticated(){
		return array("errors"=>array("No authentication found"));
	}

	public function userNotFound(){
		return array("errors"=>array("User not found"));
	}

	public function tokenExpired(){
		return array("errors"=>array("Token expired"));
	}

	public function tokenInvalid(){
		return array("errors"=>array("Token invalid"));
	}

	public function tokenNotSent(){
		return array("errors"=>array("Token not sent"));
	}

	public function artistAlreadyExists(){
		return array("errors"=>array("Artist already exists."));
	}

}