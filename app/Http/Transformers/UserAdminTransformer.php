<?php
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class UserAdminTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id'                => (int) $user->id,
            'name'              => $user->name,
            'email'             => $user->email,
        ];
    }
}