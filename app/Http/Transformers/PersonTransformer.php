<?php
namespace App\Transformers;

use App\Models\Person;
use League\Fractal\TransformerAbstract;
use App\Transformers\HeightTransformer;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;


class PersonTransformer extends TransformerAbstract
{
    public function transform(Person $person)
    {

        $phones = array();
        foreach($person->phone as $phone){
            $phones[] = ["id"=>$phone->id,"phone"=>$phone->phone];
        }

        $emails = array();
        foreach($person->email as $email){
            $emails[] = ["id"=>$email->id,"email"=>$email->email];
        }

        return [
            'id'                => $person->id,
            'firstnane'            => $person->firstnane,
            'surname'            => $person->surname,
            'email'            => $emails,
            'phone'            => $phones,
        ];
    }
}