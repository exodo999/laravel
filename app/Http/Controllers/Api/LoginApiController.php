<?php
/**
 * @resource Autentificación
 * 
 * Autenticación de usuarios
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Dingo\Api\Routing\Helpers;

use App\Transformers\UserTransformer;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\User;

class LoginApiController extends Controller
{
	use Helpers;
    /**
     *  Login
     *  
     * Si el usuario es correctamente autenticado, retorna el TOKEN JWT y el Usuario
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $error = null;
        $credentials = $request->only('email', 'password');
        $useractive = User::where("email","=",$credentials['email'])
                    ->get();

        if(!count($useractive)){
            return response()->json(['errors' => 'user_not_found'], 404);
        }

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['errors' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['errors' => 'could_not_create_token'], 500);
        }
        // fetch the user and if 
        // all good so return the token and user
        $user = JWTAuth::setToken($token)->authenticate();

        $fractal = new Manager();
        $resource = new Item($user, new UserTransformer);
        $transformedData = $fractal->createData($resource)->toArray();
        $user =$transformedData['data'];
        

        return response()->json(compact('token','errors','user'));
    }

     /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
        JWTAuth::invalidate($request->input('token'));
    }

    /**
     * Returns the authenticated user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticatedUser(Request $request)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        $errors = null;

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user','errors'));
    }


    /**
     * Refresh the token
     *
     * @return mixed
     */
    public function getToken()
    {
        $token = JWTAuth::getToken();
        if (!$token) {
            return $this->response->errorMethodNotAllowed('Token not provided');
        }
        try {
            $refreshedToken = JWTAuth::refresh($token);
        } catch (JWTException $e) {
            return $this->response->errorInternal('Not able to refresh Token');
        }
        return $this->response->withArray(['token' => $refreshedToken]);
    }

}
