<?php
/**
 * @resource Autentificación
 * 
 * Autenticación de usuarios
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Dingo\Api\Routing\Helpers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Models\Person;
use App\Models\Phone;
use App\Models\Email;

use App\Transformers\PersonTransformer;
use App\Helpers\ErrorResponses;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class PersonsApiController extends Controller
{

    use Helpers;
    use ErrorResponses;
    
    public function __construct(){
    }

    public function index()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json( $this->userNotFound(), 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json( $this->tokenExpired(), 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json( $this->tokenInvalid(), 400);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json( $this->tokenNotSent(), 500);
        }

        $error = null;

        $persons = Person::all();
        
        $fractal = new Manager();
        $resource = new Collection($persons, new PersonTransformer);
        $transformedData = $fractal->createData($resource)->toArray();

        $data = array();
        $data['errors'] = null;
        $data['data'] = $transformedData['data'];
        return response()->json($data);
    }

    public function create(Request $request)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json( $this->userNotFound(), 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json( $this->tokenExpired(), 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json( $this->tokenInvalid(), 400);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json( $this->tokenNotSent(), 500);
        }

        $error = null;
        $persondata = $request->only('firstname', 'surname', 'phone', 'email');

        $person = new Person();
        $person->firstname = $persondata['firstname'];
        $person->surname = $persondata['surname'];
        $person->save();

        foreach ($persondata['email'] as $email){
            $person->email()->create(["email"=>$email]);
        }

        foreach ($persondata['phone'] as $phone){
            $person->phone()->create(["phone"=>$phone]);
        }

        $fractal = new Manager();
        $resource = new Item($person, new PersonTransformer);
        $transformedData = $fractal->createData($resource)->toArray();

        $data = array();
        $data['errors'] = null;
        $data['data'] = $transformedData['data'];
        return response()->json($data);
    }

    public function view($personid){
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json( $this->userNotFound(), 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json( $this->tokenExpired(), 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json( $this->tokenInvalid(), 400);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json( $this->tokenNotSent(), 500);
        }

        $person = Person::find($personid);
        if($person){
            $fractal = new Manager();
            $resource = new Item($person, new PersonTransformer);
            $transformedData = $fractal->createData($resource)->toArray();

            $data = array();
            $data['errors'] = null;
            $data['data'] = $transformedData['data'];
            return response()->json($data);
        }

        return response("",404);
    }

    public function destroy($personid){
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json( $this->userNotFound(), 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json( $this->tokenExpired(), 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json( $this->tokenInvalid(), 400);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json( $this->tokenNotSent(), 500);
        }
        
        $person = Person::find($personid);
        if($person){
            $person->delete();
            return response("",200);
        }

        return response("",404);
    }

}
