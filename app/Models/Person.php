<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Person extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'persons';
    
    protected $fillable = [
        'firstname', 'surname'
    ];

    public function email()
    {
        return $this->hasMany('App\Models\Email');
    }

    public function phone()
    {
        return $this->hasMany('App\Models\Phone');
    }
}
