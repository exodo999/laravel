<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Phone extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'phones';
    
    protected $fillable = [
        'phone'
    ];
}