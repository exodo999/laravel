<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('/', function () {
        return ['API' => 'Mongo DB Test 1.0'];
    });

    /**
     * Creates a new user
     */
    $api->post('users',["uses"=>"App\Http\Controllers\Api\UserApiController@store"]);

    /**
     * Login
     */

    $api->post('authenticate', 'App\Http\Controllers\Api\LoginApiController@authenticate');
    
    /**
     * Get All Persons
     */
    $api->get('/person',["uses"=>"App\Http\Controllers\Api\PersonsApiController@index"]);

    /**
     * Creates a new Person
     */
    $api->post('/person',["uses"=>"App\Http\Controllers\Api\PersonsApiController@create"]);

    /**
     * View one person
     */
    $api->get('/person/{personid}',["uses"=>"App\Http\Controllers\Api\PersonsApiController@view"]);

    /**
     * Delete one person
     */
    $api->delete('/person/{personid}',["uses"=>"App\Http\Controllers\Api\PersonsApiController@destroy"]);
    

});