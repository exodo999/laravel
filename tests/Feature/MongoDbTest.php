<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Person;
use App\Models\Phone;
use App\Models\Email;

class MongoDbTest extends TestCase
{
    private $headers = ['HTTP_Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoZW50aWNhdGUiLCJpYXQiOjE1NTQzNTExODYsImV4cCI6MTU1NDM1NDc4NiwibmJmIjoxNTU0MzUxMTg2LCJqdGkiOiJxdUpQcENlMFk0R3FQUUpUIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.NbN9uCMPfWYWwLAklygWycgCWafcFTH2p-XPnZrOSiY'];

    public function testAddTwoPhone(){
        $person = new Person();
        $person->firstname = "Ismael";
        $person->surname = "Martinez";
        $person->save();

        $phone = new Phone();
        $phone->phone = "3310609565";
        $phone->save();

        $person->phone()->create(["phone"=>"33333333333"]);
        $person->phone()->save($phone);

        $this->assertTrue(count($person->phone) == 2);
    }

    public function testAddTwoEmails(){
        $person = new Person();
        $person->firstname = "Ismael";
        $person->surname = "Martinez";
        $person->save();

        $email = new Email();
        $email->email = "exodo999@gmail.com";
        $email->save();

        $person->email()->create(["email"=>"admin@scripmatico.com"]);
        $person->email()->save($email);

        $this->assertTrue(count($person->email) == 2);
    }

    public function testCreateUser(){
        $response = $this->json('POST', '/api/person', ['firstname' => 'Ismael', 'surname'=>'Martinez','email'=>["exodo999@gmail.com","admin@scriptmatico.com"],'phone'=>['3310609565','1111111111']],$this->headers);

        $response
            ->assertStatus(200)
            ->assertJsonCount(5, "data");

    }

    public function testAllPersons(){
        $response = $this->withHeaders($this->headers)->json('GET', '/api/person',$this->headers);

        $response
            ->assertStatus(200);

        $content = (array)json_decode($response->getContent());

        $responseOK = false;
        if(count($content['data'])>0 | count($content['data'])==0){$responseOK=true;}
        $this->assertTrue($responseOK);
    }

    public function testDeletePerson(){

        $response = $this->json('POST', '/api/person', ['firstname' => 'Ismael', 'surname'=>'Martinez','email'=>["exodo999@gmail.com","admin@scriptmatico.com"],'phone'=>['3310609565','1111111111']],$this->headers);

        $response
            ->assertStatus(200)
            ->assertJsonCount(5, "data");

        $response = $this->withHeaders($this->headers)->json('GET', '/api/person',$this->headers);

        $response
            ->assertStatus(200);

        $content = (array)json_decode($response->getContent());

        $responseOK = false;
        if(count($content['data'])>0 ){
            $person = $content['data'][0];
            $response = $this->json('DELETE', '/api/person/'.$person->id, $this->headers);
            $response->assertStatus(200);

            $response = $this->json('DELETE', '/api/person/'.$person->id, $this->headers);
            $response->assertStatus(404);
        }

    }
}
